from tkinter import *
from frontEnd import *

# Resolution = 1024 x 600

# Default User: pi
# Default Password: raspberry

def main():
    root = Tk()
    root.geometry("1024x600")
    root.title("Variable Load Interface")
    root.config(cursor=NONE)
    root.tk.call('wm', 'iconphoto', root._w, PhotoImage(file="./resources/icon.png"))
    # root.attributes("-fullscreen", True)

    # Allow application to take whole (1x1) grid
    root.grid_rowconfigure(0, weight=1)
    root.grid_columnconfigure(0,weight=1)

    app = frontEnd(master = root)
    app.grid(row=0, column=0, sticky=NSEW)
    app.mainloop()
    app.destroy()
    root.destroy()

if(__name__ == "__main__"):
    main()