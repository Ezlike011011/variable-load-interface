SIMACRO = ["","k","M","G","T","P","E","Z","Y"]
SIMICRO = ["","m","μ","n","p","f","a","z","y"]

def compactValue(value):
    index = 0
    if(0 < value < 1):
        while((value <= 1/10**2) or (index == 8)):
            value *= 10**3
            index += 1

        return value, SIMICRO[index]
    else:
        while((value >= 10**3) or (index == 8)):
            value /= 10**3
            index += 1

        return value, SIMACRO[index]