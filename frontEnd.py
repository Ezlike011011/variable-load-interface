from tkinter import *


from navbar import *
from resistorPanes import *
from loadDiagram import *

# |-------------------------|
# |                         |
# |          Pane           |
# |                         |
# |-------------------------|
# | <     Current view     >|
# |-------------------------|

class frontEnd(Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.currentScreenIndex = 0


        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0,weight=1)

        self.screens = []
        
        self.buildWidgets()

    def buildWidgets(self):
        resistorPanes = ResistorPaneCollection(self)
        resistorPanes.panes.append(ResistorPane(resistorPanes,id="A"))
        resistorPanes.panes.append(ResistorPane(resistorPanes,id="B"))
        resistorPanes.panes.append(ResistorPane(resistorPanes,id="C"))
        resistorPanes.masterPane = ResistorPane(resistorPanes,id="Master", 
            upCallback= lambda id, count: resistorPanes.snapOrAdjust(mode=TOP, targetCount=count - 1), 
            downCallback= lambda id, count: resistorPanes.snapOrAdjust(mode=BOTTOM, targetCount=count + 1),
            showDisplay=False)
        resistorPanes.buildWidgets()
        self.screens.append(("Phase-Load configuration", resistorPanes))
        
        loadDiagram = LoadDiagram(self)
        self.screens.append(("Load Diagram", loadDiagram))

        button = Button(self, text="QUIT", command = lambda: exit())
        self.screens.append(("Settings", button))
        
        self.navbar = navigationBar([screen[0] for screen in self.screens], self, lambda index: self.changeScreen(index))
    
        self.navbar.grid(row=1, column=0, sticky=NSEW)
        self.screens[0][1].grid(row=0, column=0, sticky=NSEW)

    def changeScreen(self, id):
        self.screens[self.currentScreenIndex][1].grid_forget()
        self.screens[id][1].grid(row=0, column=0, sticky=NSEW)

        self.currentScreenIndex = id
