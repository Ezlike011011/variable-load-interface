from helpers import compactValue
from tkinter import *
from tkinter import ttk

class ResistorPaneCollection(Frame):
    def __init__(self, master = None, panes = [], masterPane = None):
        super().__init__(master)
        self.master = master
        self.panes = panes
        self.masterPane = masterPane

    def buildWidgets(self):
        self.rowconfigure(0, weight=1)

        for i, pane in enumerate(self.panes):
            self.grid_columnconfigure(2*i, weight=1)
            self.grid_columnconfigure(2*i+1, weight = 0)
            pane.grid(row=0, column=2*i, sticky=NSEW)
            ttk.Separator(self, orient=VERTICAL).grid(row=0, column=(2*i)+1, sticky=NS)
        
        self.grid_columnconfigure(2*i + 2, weight = 1)
        self.masterPane.grid(row=0, column = 2*i + 2, sticky=NSEW)

    def getMax(self):
        return max([pane.count.get() for pane in self.panes])

    def getMin(self):
        return min([pane.count.get() for pane in self.panes])

    def snapOrAdjust(self, mode = TOP, targetCount = None):
        if(mode == TOP):
            adjustValue = self.getMax()
        elif(mode == BOTTOM):
            adjustValue = self.getMin()
        else:
            raise Exception("Invalid qualifier passed")

        if(all([(pane.count.get() == targetCount) for pane in self.panes])):
            #All panes are the same, inc/dec
            for pane in self.panes:
                pane.adjustResistorCount(mode)
        else:
            for pane in self.panes:
                pane.setCount(adjustValue)

            self.masterPane.setCount(adjustValue)


class ResistorPane(Frame):
    def __init__(self, master = None, bounds = (0,16), resistance = 100, \
        upCallback = None, downCallback = None, callBack = None, id = None, showDisplay = True):

        super().__init__(master)
        
        self.id = id
        self.master = master

        self.upArrow = PhotoImage(file="./resources/upArrow.png")
        self.downArrow = PhotoImage(file="./resources/downArrow.png")
        
        self.bounds = bounds
        self.resistance = resistance
        self.count = IntVar(self.bounds[0])
        self.totalStr = StringVar()
        self.totalStr.set("Total:    0Ω")

        self.showDisplay = showDisplay

        if(callBack):
            self.upCallback = callBack
            self.downCallback = callBack
        else:
            self.upCallback = upCallback
            self.downCallback = downCallback

        self.buildWidgets()

    def buildWidgets(self):

        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(0, weight=1)
        self.grid_rowconfigure(1, weight=5)
        self.grid_rowconfigure(2, weight=5)

        if(self.id):
            self.label = Label(self, text=self.id, justify=CENTER, anchor=CENTER)
        
        if(self.showDisplay):
            self.display = ttk.Progressbar(self, maximum = self.bounds[1], variable = self.count)
            self.total = Label(self, textvar = self.totalStr, justify=CENTER, anchor=CENTER)
        
        self.upButton = Button(self, text = "^", image=self.upArrow, relief = FLAT, command = lambda: self.adjustResistorCount(TOP))
        self.downButton = Button(self, text = "v", image=self.downArrow, relief = FLAT, command = lambda: self.adjustResistorCount(BOTTOM))


        self.upButton.config(activebackground=self.upButton.cget('background'))
        self.downButton.config(activebackground=self.downButton.cget('background'))

        if(self.label):
            self.label.grid(row=0, column=0, sticky=NSEW, pady=5)

        if(self.showDisplay):
            self.grid_rowconfigure(3, weight=5)
            self.grid_rowconfigure(4, weight=5)
            self.display.grid(row=1, column=0, sticky=NSEW, padx=10, pady=10)
            self.total.grid(row=2, column=0)
            self.upButton.grid(row=3, column=0, sticky=NSEW, padx=35, pady=20)
            self.downButton.grid(row=4, column=0, sticky=NSEW, padx=35, pady=20)
        else:
            self.upButton.grid(row=1, column=0, sticky=NSEW, padx=35, pady=20)
            self.downButton.grid(row=2, column=0, sticky=NSEW, padx=35, pady=20)



    def adjustResistorCount(self, direction):
        if(direction == TOP):
            amount = 1
        elif(direction == BOTTOM):
            amount = -1
        else:
            raise Exception("Invalid direction passed")

        if(self.count.get() + amount > self.bounds[1]):
            self.setCount(self.bounds[1])
        elif(self.count.get() + amount < self.bounds[0]):
            self.setCount(self.bounds[0])
        else:
            self.setCount(self.count.get() + amount)

        if((direction == TOP) and (self.upCallback)):
            self.upCallback(id, self.count.get())
        elif((direction == BOTTOM) and (self.downCallback)):
            self.downCallback(id, self.count.get())

    def setCount(self, amount):
        self.count.set(amount)

        self.updateTotal()

    def updateTotal(self):
        value, prefix = compactValue(self.count.get() * self.resistance)
        self.totalStr.set(f"Total: {value}{prefix}Ω")

        