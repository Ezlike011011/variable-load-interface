from tkinter import *
from tkinter import ttk

class navigationBar(Frame):
    def __init__(self, options, master=None, callback=None):
        super().__init__(master)
        self.master = master

        self.callback = callback

        self.currentText = StringVar()
        self.leftArrow = PhotoImage(file="./resources/leftArrow.png")
        self.rightArrow = PhotoImage(file="./resources/rightArrow.png")

        self.options = options
        self.index = 0

        self.buildWidgets()

    def buildWidgets(self):
        self.currentText.set(self.options[self.index])
        self.leftButton = Button(self, text="<", image=self.leftArrow, relief=FLAT, command = lambda: self.walk(LEFT))
        self.rightButton = Button(self, text=">", image=self.rightArrow , relief=FLAT, command = lambda: self.walk(RIGHT))
        self.text = Label(self, textvariable=self.currentText, justify = CENTER, anchor = CENTER)

        self.grid_columnconfigure(0, weight = 1)
        self.grid_columnconfigure(1, weight = 1)
        self.grid_columnconfigure(2, weight = 1)
        self.grid_rowconfigure(0, weight=1)
        self.grid_rowconfigure(1, weight=1)


        self.rightButton.config(activebackground=self.rightButton.cget('background'))
        self.leftButton.config(activebackground=self.leftButton.cget('background'))

        ttk.Separator(self, orient=HORIZONTAL).grid(row=0, column=0, columnspan=3, sticky=EW)
        self.leftButton.grid(row=1, column=0, sticky=N + W + S)
        self.text.grid(row=1, column=1, sticky=N + E + W + S)
        self.rightButton.grid(row=1, column=2, sticky=N + E + S)



    def walk(self, direction):
        if(direction == LEFT):
            self.index = self.index - 1
        elif(direction == RIGHT):
            self.index = self.index + 1
        else:
            raise Exception("Invalid direction passed")

        # Calculate the new index and set the string
        self.index = self.index % len(self.options)
        self.currentText.set(self.options[self.index])

        self.callback(self.index)