from tkinter import *
from tkinter import ttk
from helpers import compactValue

class LoadDiagram(Frame):
    def __init__(self, master):
        super().__init__(master)
        self.sourceVoltage = StringVar()
        self.sourceFreq = StringVar()

        self.inverterState = StringVar()
        self.inverterTemperature = StringVar()
        self.inverterPower = StringVar()

        self.batteryVoltage = StringVar()
        self.batteryCharge = StringVar()

        self.diagram = PhotoImage(file="./resources/diagram.png")

        self.sourceVoltage.set("Voltage:    0V")
        self.sourceFreq.set("Frequency:    0Hz")

        self.inverterState.set("Discharging")
        self.inverterTemperature.set("Temperature:    0°C")
        self.inverterPower.set("Power:    0W")

        self.batteryVoltage.set("Voltage:    0V")
        self.batteryCharge.set("Charge:    0%")

        self.buildWidgets()

    def buildWidgets(self):
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.columnconfigure(2, weight=1)

        self.rowconfigure(0, weight=0)
        self.rowconfigure(1, weight=0)
        self.rowconfigure(2, weight=0)
        self.rowconfigure(3, weight=0)
        self.rowconfigure(4, weight=0)

        self.sourceVoltageLabel = Label(self, textvariable=self.sourceVoltage, justify=CENTER, anchor=CENTER, padx = 50)
        self.sourceFreqLabel = Label(self, textvariable=self.sourceFreq, justify=CENTER, anchor=CENTER, padx = 50)

        self.inverterStateLabel = Label(self, textvariable=self.inverterState, justify=CENTER, anchor=CENTER)
        self.inverterTemperatureLabel = Label(self, textvariable=self.inverterTemperature, justify=CENTER, anchor=CENTER)
        self.inverterPowerLabel = Label(self, textvariable=self.inverterPower, justify=CENTER, anchor=CENTER)

        self.batteryVoltageLabel = Label(self, textvariable=self.batteryVoltage, justify=CENTER, anchor=CENTER, padx = 50)
        self.batteryChargeLabel = Label(self, textvariable=self.batteryCharge,  justify=CENTER, anchor=CENTER, padx = 50)

        Label(self, image=self.diagram).grid(row = 0, column = 0, columnspan = 3, sticky=NE + W)

        ttk.Separator(self, orient=HORIZONTAL).grid(row=1, column=0, columnspan = 3, sticky=EW)

        self.sourceVoltageLabel.grid(row = 2, column = 0, sticky = NW)
        self.sourceFreqLabel.grid(row = 3, column = 0, sticky = NW)
        self.inverterStateLabel.grid(row = 2, column = 1, sticky = NE + W)
        self.inverterTemperatureLabel.grid(row = 3, column = 1, sticky = NE + W)
        self.inverterPowerLabel.grid(row = 4, column = 1, sticky = NE + W)
        self.batteryVoltageLabel.grid(row = 2, column = 2, sticky = NE)
        self.batteryChargeLabel.grid(row = 3, column = 2, sticky = NE)

    def setParam(self, label, value, unit, variable):
        value, prefix = compactValue(value)
        variable.set(f"{label}: {value}{prefix}{unit}")

    def updateValues(self):
        # TODO: interface with ups system to get data
        
        __ = 0
        self.setParam("Voltage", __, "V", self.sourceVoltage)
        self.setParam("Frequency", __, "Hz", self.sourceVoltage)
        self.setParam("Temperature", __, "°C", self.sourceVoltage)
        self.setParam("Power", __, "W", self.sourceVoltage)
        self.setParam("Voltage", __, "V", self.sourceVoltage)
        self.setParam("Charge", __, "%", self.sourceVoltage)
